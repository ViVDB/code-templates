%% Script to format an image nicely, without forgetting anything (hopefully)

figure;
clf

set(gcf,'defaulttextinterpreter','latex');
% set(gcf,'defaulttextinterpreter','tex'); % sometimes better for Octave
% depending on the interpreter, may need to add or no the "$" around math
% degree : \deg
set(gcf,'paperunits','centimeters');
set(gcf,'Units','centimeters');

set(gcf,'position',[0,0,21,13]);
% Useful only when printed
set(gcf,'papersize',[21,13]);
% set(gcf,'position',[1,1,10.5,10.5]);
% set(gcf,'papersize',[10.5,10.5]);
% set(gcf,'position',[0,0,35,25]);
% set(gcf,'papersize',[35,25]);
% Numbers may need adjustment, at least using Octave, depending on the size of
% the legend for example
% orient(gcf,'landscape') % can be used too to adjust things, in Octave
%
% Had one working example with Octave using:
%   set(gcf,'position',[0,0,15.2,9.2]);
%   set(gcf,'papersize',[21,13]);
% but it may also depend on screen resolution and other things
% I don't quite get, and the fact I use a tiling window manager.

plot(1:10,0.1:0.1:1,'-x',1:10,0.2:0.1:1.1,'-x')
legend('data 1','data 2','Location','northeast')
xlabel('abscisse -- see imageTemplate.m in code !');
ylabel('ordonn\''{e}e');

axis([0.5,10.5,0,1.5]);
% xlim([0.5,10.5]);
% ylim([0,1.5]);

saveas(gcf,'../results/dataTemplate2.svg')
% Warning : including pdf files works for LaTex documents,
% not for markdown documents !!!!
% svg files do not work in tex documents
print -dpdf ../results/dataTemplate2.pdf
% print(gcf,'../results/dataTemplate2.pdf',"-dpdf") % can be use to change name
                                                    % programatically

close(gcf);
